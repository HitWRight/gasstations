﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			string myJson = "{\"Username\":\"Tautvydas\",\"Password\":\"pass\"}";
			using (var client = new HttpClient())
			{
				var response = client.PostAsync(
					"http://aurora.acm.lt:11116/api/Account/Login",
					new StringContent(myJson, Encoding.UTF8, "application/json")).Result;

				myJson = "{\"GasType\": 0, \"Price\":0.8}";
				response = client.PostAsync(
					"http://aurora.acm.lt:11116/api/GasPrice/3",
					new StringContent(myJson, Encoding.UTF8, "application/json")).Result;

				response = client.GetAsync("http://aurora.acm.lt:11116/api/Stations").Result;
				Console.WriteLine(response.Content.ReadAsStringAsync().Result);
			}

		}
	}
}
