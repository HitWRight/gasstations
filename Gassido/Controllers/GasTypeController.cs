﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GasStations.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Gassido.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[AllowAnonymous]
	public class GasTypeController : ControllerBase
	{
		[HttpGet]
		public ActionResult<IEnumerable<ModelGasType>> Get()
		{
			return new[]
			{
				new ModelGasType() {GasType = (int) GasType.B95, Name = "Benzinas 95"},
				new ModelGasType() {GasType = (int) GasType.B98, Name = "Benzinas 98"},
				new ModelGasType() {GasType = (int) GasType.Diesel, Name = "Dyzelinas"}
			};
		}
	}

	public struct ModelGasType
	{
		public int GasType { get; set; }
		public string Name { get; set; }
		
	}
}
