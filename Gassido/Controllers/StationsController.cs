﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GasStations.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Gassido.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class StationsController : ControllerBase
	{
		private readonly Context _context;

		public StationsController(Context context)
		{
			_context = context;
		}
		// GET api/values
		[HttpGet]
		public ActionResult<IEnumerable<GasStation>> Get() => _context.Stations.Include(s => s.Owner).ToList();

		[AllowAnonymous]
		[HttpGet("{fuelType}/{lat}/{lng}/{dist}")]
		public ActionResult<GasStation> Get(int fuelType, float lng, float lat, double dist)
		{
			var a = _context.Stations.Where(st =>
				distance(lat, lng, st.Latitude, st.Longitude, 'K') < dist
			).Select(st => st.Id).ToList();
			var b = _context.GasPrices.Where(g => a.Contains(g.GasStationId) && (int) g.GasType == fuelType);
			var cheapestStation = b.OrderBy(ad => ad.Price).FirstOrDefault();
			if (cheapestStation == null)
				return null;
			return _context.Stations.Find(cheapestStation.GasStationId);
		}

		private double distance(double lat1, double lon1, double lat2, double lon2, char unit)
		{
			if ((lat1 == lat2) && (lon1 == lon2))
			{
				return 0;
			}
			else
			{
				double theta = lon1 - lon2;
				double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
				dist = Math.Acos(dist);
				dist = rad2deg(dist);
				dist = dist * 60 * 1.1515;
				if (unit == 'K')
				{
					dist = dist * 1.609344;
				}
				else if (unit == 'N')
				{
					dist = dist * 0.8684;
				}
				return (dist);
			}
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts decimal degrees to radians             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private double deg2rad(double deg)
		{
			return (deg * Math.PI / 180.0);
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts radians to decimal degrees             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private double rad2deg(double rad)
		{
			return (rad / Math.PI * 180.0);
		}
	}
}
