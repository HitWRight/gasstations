﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GasStations.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KGHelpDesk.Server.Controllers
{
	[AllowAnonymous]
	[Route("api/[controller]")]
	public class AccountController : Controller
	{
		private readonly Context _context;

		public AccountController(Context context)
		{
			_context = context;
		}

		[HttpGet]
		public ActionResult<string> Get()
		{
			return Ok(HttpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Name)?.Value ?? "");
		}

		[HttpPost("Login")]
		public async Task<IActionResult> Login([FromBody]User account)
		{
			var user = _context.Users.SingleOrDefault(u => u.Username == account.Username && u.Password == account.Password);
			if (user != null)
			{
				await HttpContext.SignInAsync(
					new ClaimsPrincipal(new ClaimsIdentity(new[]
					{
						new Claim(ClaimTypes.Name, account.Username),
						new Claim(ClaimTypes.Sid, user.Id.ToString()),
					}, CookieAuthenticationDefaults.AuthenticationScheme)));
				return Ok();
			}

			return BadRequest();
		}

		[HttpPost("Logout")]
		public async Task<IActionResult> Logout()
		{
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			return Ok();
		}

		public static string GetUser(int userId)
		{
			using (var conn =
				new SqlConnection(
					"Server=10.0.110.249;Database=AURORADB;User Id=HitWRight;password=medialandas-puikus-PARTNERIS"))
			{
				SqlCommand command = new SqlCommand("select N10_FirstName from N10_Users where N10_UID = @uid", conn);
				command.Parameters.AddWithValue("@uid", userId);
				try
				{
					conn.Open();
					using (var reader = command.ExecuteReader())
					{
						if (reader.HasRows && reader.Read())
							return reader[0].ToString();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}
			return "";
		}
	}
}
