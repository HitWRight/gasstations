﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GasStations.Models
{
	public class GasPrice
	{
		public DateTime Registered { get; set; }

		[ForeignKey("GasStation")]
		public int GasStationId { get; set; }
		public GasStation GasStation { get; set; }
		public GasType GasType { get; set; }

		public decimal Price { get; set; }
		public User User { get; set; }
	}
}