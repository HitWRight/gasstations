﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Gassido.Migrations
{
    public partial class GasPricesToFloat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "Price",
                table: "GasPrices",
                nullable: false,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "GasPrices",
                nullable: false,
                oldClrType: typeof(float));
        }
    }
}
