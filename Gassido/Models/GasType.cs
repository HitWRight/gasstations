﻿namespace GasStations.Models
{
	public enum GasType
	{
		B95,
		B98,
		Diesel
	}
}