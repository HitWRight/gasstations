﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GasStations.Models
{
	public class Context : DbContext
	{
		public DbSet<User> Users { get; set; }
		public DbSet<Company> Companies { get; set; }
		public DbSet<GasStation> Stations { get; set; }
		//public DbSet<GasPrice> GasPrices { get; set; } 

		public Context() : base()
		{
			
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer("Server=127.0.0.1;Database=GasStations;User Id=HitWRight;Password=X9Baibokas;");
			base.OnConfiguring(optionsBuilder);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<GasPrice>().HasKey(gp => new {gp.Registered, gp.GasStationId, gp.GasType});
			base.OnModelCreating(modelBuilder);
		}
	}
}
