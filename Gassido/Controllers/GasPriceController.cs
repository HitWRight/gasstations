﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GasStations.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Gassido.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class GasPriceController : ControllerBase
	{
		private readonly Context _context;

		public GasPriceController(Context context)
		{
			_context = context;
		}

		[AllowAnonymous]
		[HttpGet("{stationId}")]
		public ActionResult<IEnumerable<ModelA>> Get(int stationId)
		{
			var prices = _context.GasPrices.Where(gp => gp.GasStationId == stationId).OrderByDescending(gp => gp.Registered).ToList();

			
			return new ModelA[] {new ModelA(){Name = "B95", Value = prices.FirstOrDefault(p => p.GasType == GasType.B95).Price },
				new ModelA(){Name = "B98", Value = prices.FirstOrDefault(p => p.GasType == GasType.B98).Price },
				new ModelA(){Name = "Diesel", Value = prices.FirstOrDefault(p => p.GasType == GasType.Diesel).Price }};
		}

		

		[HttpPost("{stationId}")]
		public void Post(int stationId, [FromBody]GasPrice gasPrice)
		{
			_context.GasPrices.Add(new GasPrice()
			{
				GasStation = _context.Stations.Single(s => s.Id == stationId), GasType = gasPrice.GasType, Price = gasPrice.Price, Registered = DateTime.Now,
				User = _context.Users.Find(Convert.ToInt32(HttpContext.User.Claims.Single(c => c.Type == ClaimTypes.Sid).Value))
			});
			_context.SaveChanges();
		}

		[HttpPut("{id}")]
		public void Put(int id, [FromBody] string value)
		{
		}

		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}
	}

	public struct ModelA
	{
		public string Name { get; set; }
		public float Value { get; set; }
	}
}
