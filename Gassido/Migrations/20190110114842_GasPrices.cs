﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Gassido.Migrations
{
    public partial class GasPrices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GasPrice_Stations_GasStationId",
                table: "GasPrice");

            migrationBuilder.DropForeignKey(
                name: "FK_GasPrice_Users_UserId",
                table: "GasPrice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GasPrice",
                table: "GasPrice");

            migrationBuilder.RenameTable(
                name: "GasPrice",
                newName: "GasPrices");

            migrationBuilder.RenameIndex(
                name: "IX_GasPrice_UserId",
                table: "GasPrices",
                newName: "IX_GasPrices_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_GasPrice_GasStationId",
                table: "GasPrices",
                newName: "IX_GasPrices_GasStationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GasPrices",
                table: "GasPrices",
                columns: new[] { "Registered", "GasStationId", "GasType" });

            migrationBuilder.AddForeignKey(
                name: "FK_GasPrices_Stations_GasStationId",
                table: "GasPrices",
                column: "GasStationId",
                principalTable: "Stations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GasPrices_Users_UserId",
                table: "GasPrices",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GasPrices_Stations_GasStationId",
                table: "GasPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_GasPrices_Users_UserId",
                table: "GasPrices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GasPrices",
                table: "GasPrices");

            migrationBuilder.RenameTable(
                name: "GasPrices",
                newName: "GasPrice");

            migrationBuilder.RenameIndex(
                name: "IX_GasPrices_UserId",
                table: "GasPrice",
                newName: "IX_GasPrice_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_GasPrices_GasStationId",
                table: "GasPrice",
                newName: "IX_GasPrice_GasStationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GasPrice",
                table: "GasPrice",
                columns: new[] { "Registered", "GasStationId", "GasType" });

            migrationBuilder.AddForeignKey(
                name: "FK_GasPrice_Stations_GasStationId",
                table: "GasPrice",
                column: "GasStationId",
                principalTable: "Stations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GasPrice_Users_UserId",
                table: "GasPrice",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
